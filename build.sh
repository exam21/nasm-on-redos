#!/bin/bash

FILENAME=$1

rm -f $FILENAME.o
rm -f $FILENAME

nasm -f elf64 $FILENAME.asm
ld -m elf_x86_64 $FILENAME.o -o $FILENAME
./$FILENAME
